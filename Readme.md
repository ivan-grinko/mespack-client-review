# Назначение

Сборка содержит класс BasicHttpClient, который облегчает асинхронное выполнение HTTP-запросов, реализуя следующие функции:

- Использование MessagePack для сериализации / десериализации;
- Автоматическое конвертирование UTC дат в местное время и обратно в случае когда DTO реализуют интерфейс IDatesConvertable;
- OpenID Connect для аутентификации пользователей;
- Уведомление о прогрессе выполнения запросов с помощью IProgress<int> при загрузке/скачивания файлов.

## Описание

Изначально планировалось использовать для организации межсервисного взаимодействия, где требовалось пересылать часто массивы дат и бинарные данные, поэтому был выбран MessagePack.

На сервере даты хранятся в UTC, клиенты отправляют и желают получать их в местном времени. При отправке на сервер MessagePack преобразует даты в UTC формат, а при получении дат от сервера - BasicHttpClient, если DTO реализуют интерфейс IDatesConvertable, самостоятельно вызывает нужное преобразование.

Со временем потребовалось добавить клиентское desktop-приложение. Поэтому для аутентификации добавлено опциональное использование OpenID Connect (IdentityServer4 на стороне сервера) и возможность отображения прогресса при пересылке файлов.

## Примеры использования

```java
var _client = new BasicHttpClient();  
```

Получение DTO от сервера:

```java
var result = await _client.Request<TDTO>(RequestTypes.Get, "https://some.endpoint");
```

Отправка DTO  на сервер:

```java
// Где-то выше создан:
// TInputDto inputDto;
var result = await _client.Request<TOutputDTO, TInputDto>(RequestTypes.Post, "https://some.endpoint", inputDto);
```

Скачивание файла с уведомлением о прогрессе и возможностью отменить запрос:

```java
// Где-то выше были созданы:
// IProgress<int> progress;
// CancellationToken token;
var inputStream = await _client.RequestStream(RequestTypes.Get, "https://some.endpoint", null, progress, token);
if (inputStream != null)
{
    // ... действия с потоком
    inputStream.Dispose();
}
```

Отправка файла c DTO на сервер  с уведомлением о прогрессе и возможностью отменить запрос:

```java
// Где-то выше были созданы:
// IProgress<int> progress;
// CancellationToken token;

var content = new MultipartFormDataContent();

// dto - объект, который нужно передать вместе с файлом.
var dtoContent = content.AddJsonSerializedItem("название_поля_dto", dto);       

// Поток с данными файла:
// Stream fileStream;
// Имя файла:
// string fileName;
var response = await _client.UploadStream<TOutputDto>("https://some.endpoint", content, fileStream, "название_поля_файла", "имя_файла.расширение", null, null, progress, token);

// response будет null при ошибке или DTO-ответ от сервера типа TOutputDto.

dtoContent.Dispose();
content.Dispose();
```

