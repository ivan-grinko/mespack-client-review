﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace Review.MesPackClient
{
    /// <summary>
    /// Вспомогательные функции для работы с составными запросами (MultipartFormData).
    /// </summary>
    public static class MultipartFormDataContentExtensions
    {
        /// <summary>
        /// Добавить данные в формате JSON в составной запрос.
        /// </summary>
        /// <param name="content">Составной запрос.</param>
        /// <param name="fieldName">Название ключа.</param>
        /// <param name="item">Данные.</param>
        /// <returns>Тело запроса.</returns>
        public static HttpContent AddJsonSerializedItem(this MultipartFormDataContent content, string fieldName, object item)
        {
            var serializedDto = JsonConvert.SerializeObject(item);
            var httpContent = new StringContent(serializedDto);

            content.Add(httpContent, fieldName);
            return httpContent;
        }
    }
}
