﻿using System;
using System.IO;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;

using IdentityModel.OidcClient;

using System.Threading;
using System.Net.Http.Handlers;

namespace Review.MesPackClient
{
    /// <summary>
    /// Перечисление HTTP запросов.
    /// </summary>
    public enum RequestTypes
    {
        Get,
        Post,
        Put,
        Delete
    }

    /// <summary>
    /// Класс cодержит методы для облегчения конструирования HTTP-запросов.
    /// </summary>
    /// <remarks>
    /// Уведомляет о прогрессе выполнения запросов с помощью <see cref="IProgress{T}"/>.
    /// Поддерживает MessagePack и OpenId Connect.
    /// </remarks>
    public partial class BasicHttpClient
    {
        /// <summary>
        /// HTTP клиент.
        /// </summary>
        private readonly HttpClient _client;

        /// <summary>
        /// Клиент для OpenId Connect.
        /// </summary>
        private OidcClient _oidcClient;

        /// <summary>
        /// Результат прохождения авторизации.
        /// </summary>
        private LoginResult _login;

        /// <summary>
        /// Текущий токен доступа.
        /// </summary>
        private string _currentAccessToken;

        /// <summary>
        /// Текущий токен для получения нового токена доступа.
        /// </summary>
        private string _currentRefreshToken;

        /// <summary>
        /// Таймер для обновления токена доступа.
        /// </summary>
        private readonly System.Timers.Timer _tokenRefreshTimer;

        /// <summary>
        /// Вспомогательный объект для уведомления о прогрессе выполнения запросов.
        /// </summary>
        private readonly ProgressReportHelper _progressReportHelper;

        public BasicHttpClient()
        {
            _tokenRefreshTimer = new System.Timers.Timer
            {
                AutoReset = false
            };
            _tokenRefreshTimer.Elapsed += async (s, e) => await RefreshToken();            

            var processMsgHander = new ProgressMessageHandler(new HttpClientHandler());


            _progressReportHelper = new ProgressReportHelper();
            // Подписаться на события прогресса обработки HTTP-запросов.
            processMsgHander.HttpSendProgress += _progressReportHelper.HttpProgressHandler;
            processMsgHander.HttpReceiveProgress += _progressReportHelper.HttpProgressHandler;

            _client = new HttpClient(processMsgHander);

            // Включить сериализацию MessagePack.
            _client.DefaultRequestHeaders.Accept.Clear();
            _client.DefaultRequestHeaders.Accept.Add(
                new MediaTypeWithQualityHeaderValue("application/x-msgpack"));
 
        }

        ~BasicHttpClient()
        {
            _tokenRefreshTimer.Stop();
        }

        /// <summary>
        /// Выполнить HTTP запрос.
        /// </summary>
        /// <typeparam name="TOutputDto">Тип ожидаемой посылки с ответом.</typeparam>
        /// <param name="requestType">Тип запроса.</param>
        /// <param name="url">Веб адрес.</param>
        /// <param name="defaultOutput">Значение, которое использовать для возвращения при ошибке выполнения запроса.</param>
        /// <param name="queryString">Query String.</param>
        /// <param name="cancellationToken">Токен для отмены запроса.</param>
        /// <returns>Ответ от сервера.</returns>
        public async Task<TOutputDto> Request<TOutputDto>(
            RequestTypes requestType, 
            string url,
            TOutputDto defaultOutput = default,
            string queryString = null,
            CancellationToken? cancellationToken = null)
            
        {
            // Построить URL.
            var requestUrl = ConstructUrl(url, queryString);

            // Выполнить запрос.
            var response = await ConstructResponseTask(requestType, requestUrl, null, cancellationToken);

            if (!response.IsSuccessStatusCode)
            {
                return defaultOutput;
            }
            
            // Выполнить десериализацию ответа.
            var result = await DeserializeFromMessagePackAsync<TOutputDto>(response.Content);

            // Выполнить преобразование дат в местное время, если такое требуется.
            if(result is IDatesConvertable)
            {
                (result as IDatesConvertable).DatesToLocal();
            }

            return result;
        }

        /// <summary>
        /// Выполнить HTTP запрос.
        /// </summary>
        /// <typeparam name="TOutputDto">Тип ответа от сервера.</typeparam>
        /// <typeparam name="TBodyDto">Тип тела запроса.</typeparam>
        /// <param name="requestType">Тип запроса.</param>
        /// <param name="url">Веб адрес.</param>
        /// <param name="body">Тело запроса.</param>
        /// <param name="defaultOutput">Значение, которое использовать для возвращения при ошибке выполнения запроса.</param>
        /// <param name="queryString">Query String.</param>
        /// <param name="cancellationToken">Токен для отмены формирования запроса</param>
        /// <returns></returns>
        public async Task<TOutputDto> Request<TOutputDto, TBodyDto>(
            RequestTypes requestType,
            string url,
            TBodyDto body,
            TOutputDto defaultOutput = default,
            string queryString = null,
            CancellationToken? cancellationToken = null)
        {
            // Построить URL.
            var requestUrl = ConstructUrl(url, queryString);

            // Для Get и Delete тело запроса отсутствует.
            var content = requestType == RequestTypes.Get || requestType == RequestTypes.Delete
                ? null
                : SerializeToMessagePack(body);

            // Выполнить запрос.
            var response = await ConstructResponseTask(requestType, requestUrl, content, cancellationToken);

            content.Dispose();

            if (!response.IsSuccessStatusCode)
            {
                return defaultOutput;
            }

            // Выполнить десериализацию ответа.
            var result = await DeserializeFromMessagePackAsync<TOutputDto>(response.Content);

            // Выполнить преобразование дат в местное время, если такое требуется.
            if (result is IDatesConvertable)
            {
                (result as IDatesConvertable).DatesToLocal();
            }

            return result;
        }

        /// <summary>
        /// Формирует URL для запроса.
        /// </summary>
        /// <param name="baseUrl">Веб адрес.</param>
        /// <param name="queryString">Query String.</param>
        /// <returns>Готовый URL адрес.</returns>
        private string ConstructUrl(string baseUrl, string queryString = null)
        {
            var urlBulder = new UriBuilder(baseUrl);
            if (queryString != null)
            {
                urlBulder.Query = queryString;
            }
            var url = urlBulder.ToString();
            return url;
        }

        /// <summary>
        /// Создание задачи для HTTP запроса.
        /// </summary>
        /// <param name="requestType">Тип запроса.</param>
        /// <param name="url">Веб адрес.</param>
        /// <param name="content">Тело запроса.</param>
        /// <param name="cancellationToken">Токен для отмены задачи.</param>
        /// <returns>Задача для выполнения HTTP запроса.</returns>
        private Task<HttpResponseMessage> ConstructResponseTask(RequestTypes requestType, string url, 
            HttpContent content = null, CancellationToken? cancellationToken = null)
        {
            Task<HttpResponseMessage> responseTask = null;

            switch (requestType)
            {
                case RequestTypes.Get:
                    responseTask = cancellationToken == null 
                        ? _client.GetAsync(url) 
                        : _client.GetAsync(url, cancellationToken.GetValueOrDefault());
                    break;
                case RequestTypes.Delete:
                    responseTask = cancellationToken == null 
                        ? _client.DeleteAsync(url) 
                        : _client.DeleteAsync(url, cancellationToken.GetValueOrDefault());
                    break;
                case RequestTypes.Post:
                    responseTask = cancellationToken == null 
                        ? _client.PostAsync(url, content) 
                        : _client.PostAsync(url, content, cancellationToken.GetValueOrDefault());
                    break;
                case RequestTypes.Put:
                    responseTask = cancellationToken == null 
                        ? _client.PutAsync(url, content) 
                        : _client.PutAsync(url, content, cancellationToken.GetValueOrDefault());
                    break;
            }            

            return responseTask;
        }      
    }
}
