﻿using System;
using System.Collections.Generic;
using System.Net.Http.Handlers;
using System.Text;

namespace Review.MesPackClient
{
    /// <summary>
    /// Вспомогательный класс для уведомления о прогрессе выполнения HTTP-запроса.
    /// </summary>
    internal class ProgressReportHelper
    {
        public ProgressReportHelper()
        {
            _lastReportedValue = int.MinValue;
            _progress = null;
        }

        /// <summary>
        /// Установить получателя прогресса выполнения запроса.
        /// </summary>
        /// <remarks>
        /// Метод должен быть вызван, чтобы <see cref="HttpProgressHandler"/> передавал прогресс.
        /// </remarks>
        /// <param name="progress">Получатель уведомлений.</param>
        public void SetProgress(IProgress<int> progress)
        {
            _lastReportedValue = int.MinValue;
            _progress = progress;
        }

        /// <summary>
        /// Удалить получателя прогресса выполнения запроса.
        /// </summary>
        /// <remarks>
        /// Метод должен быть вызван после того как уведомлять о прогрессе больше не требуется.
        /// </remarks>
        public void ClearProgress()
        {
            _lastReportedValue = int.MinValue;
            _progress = null;
        }

        /// <summary>
        /// Обработчик события прогресса HTTP-запроса.
        /// </summary>
        /// <param name="sender">Отправитель.</param>
        /// <param name="progress">Аргументы события.</param>
        public void HttpProgressHandler(object sender, HttpProgressEventArgs progress)
        {
            if(_progress != null && progress.ProgressPercentage != _lastReportedValue)
            {
                _lastReportedValue = progress.ProgressPercentage;
                _progress.Report(_lastReportedValue);
            }
        }

        /// <summary>
        /// Получатель уведомлений.
        /// </summary>
        private IProgress<int> _progress;

        /// <summary>
        /// Последний отправленный получателю прогресс. 
        /// </summary>
        private int _lastReportedValue;
    }
}
