﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Review.MesPackClient
{
    partial class BasicHttpClient
    {
        /// <summary>
        /// Выполнить запрос, который не содержит тело, и вернуть результат булевой переменной.
        /// </summary>
        /// <param name="requestType">Тип запроса.</param>
        /// <param name="url">Веб адрес.</param>
        /// <param name="queryString">Query String.</param>
        /// <param name="cancellationToken">Токен для отмены запроса.</param>
        /// <returns>Запрос выполнен успешно или нет.</returns>
        public Task<bool> BoolResultQueryRequest(
            RequestTypes requestType,
            string url,
            string queryString = null,
            CancellationToken? cancellationToken = null)
        {
            return RequestWithDefinedTypeResult<bool>(requestType, url, true, false, queryString, cancellationToken);
        }

        /// <summary>
        /// Выполнить запрос, который имеет тело, и вернуть результат булевой переменной.
        /// </summary>
        /// <typeparam name="TBodyDto">Тип тела в запросе.</typeparam>
        /// <param name="requestType">Тип запроса.</param>
        /// <param name="url">Веб адрес.</param>
        /// <param name="body">Тело запроса.</param>
        /// <param name="queryString">Query String.</param>
        /// <param name="cancellationToken">Токен для отмены выполнения запроса.</param>
        /// <returns>Запрос выполнен успешно или нет.</returns>
        public Task<bool> BoolResultRequest<TBodyDto>(
            RequestTypes requestType,
            string url,
            TBodyDto body,
            string queryString = null,
            CancellationToken? cancellationToken = null)
        {
            return RequestWithDefinedTypeResult(requestType, url, body, true, false, queryString, cancellationToken);
        }

        /// <summary>
        /// Выполнить запрос без тела, в качестве индикатора результата использовать заданные данные.
        /// </summary>
        /// <typeparam name="TOutput">Тип результата выполнения.</typeparam>
        /// <param name="requestType">Тип запроса.</param>
        /// <param name="url">Веб адрес.</param>
        /// <param name="successOutput">Значение если запрос успешно выполнен (код 200).</param>
        /// <param name="errorOutput">Значение во всех остальных случаях.</param>
        /// <param name="queryString">Query String.</param>
        /// <param name="cancellationToken">Токен для отмены запроса/</param>
        /// <returns>Результат выполнения запроса.</returns>
        public async Task<TOutput> RequestWithDefinedTypeResult<TOutput>(
            RequestTypes requestType,
            string url,
            TOutput successOutput,
            TOutput errorOutput = default,
            string queryString = null,
            CancellationToken? cancellationToken = null)
        {
            var requestUrl = ConstructUrl(url, queryString);
            var response = await ConstructResponseTask(requestType, requestUrl, null, cancellationToken);

            return response.IsSuccessStatusCode ? successOutput : errorOutput;
        }

        /// <summary>
        /// Выполнить запрос с телом, в качестве индикатора результата использовать заданные данные.
        /// </summary>
        /// <typeparam name="TOutput">Тип результата выполнения.</typeparam>
        /// <typeparam name="TBodyDto">Тип тела в запросе.</typeparam>
        /// <param name="requestType">Тип запроса.</param>
        /// <param name="url">Веб адрес.</param>
        /// <param name="body">Тело запроса.</param>
        /// <param name="successOutput">Значение если запрос успешно выполнен (код 200)</param>
        /// <param name="errorOutput">Значение во всех остальных случаях.</param>
        /// <param name="queryString">Query String.</param>
        /// <param name="cancellationToken">Токен для отмены выполнения. запроса.</param>
        /// <returns>Результат выполнения запроса.</returns>
        public async Task<TOutput> RequestWithDefinedTypeResult<TOutput, TBodyDto>(
            RequestTypes requestType,
            string url,
            TBodyDto body,
            TOutput successOutput,
            TOutput errorOutput = default,
            string queryString = null,
            CancellationToken? cancellationToken = null)
        {
            var requestUrl = ConstructUrl(url, queryString);

            var content = requestType == RequestTypes.Get || requestType == RequestTypes.Delete
                ? null
                : SerializeToMessagePack(body);

            var response = await ConstructResponseTask(requestType, requestUrl, content, cancellationToken);

            content?.Dispose();

            return response.IsSuccessStatusCode ? successOutput : errorOutput;
        }
    }
}
