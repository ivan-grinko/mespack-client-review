﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Review.MesPackClient
{
    /// <summary>
    /// Интерфейс для объектов, которые должны выполнять преобразование дат между местным временем и UTC. 
    /// </summary>
    /// <remarks>
    /// На сервере даты хранятся в формате UTC, а на клиенте в местном.
    /// MessagePack выполняет преобразование при отправке на сервере даты в UTC автоматически.
    /// А при получении дат от сервера, они должны быть преобразованы в местное время.
    /// Данный интерфейс показывает, что объект может выполнять такое преобразование всех своих дат.
    /// </remarks>
    public interface IDatesConvertable
    {
        /// <summary>
        /// Преобразовать даты в объекте в местное время.
        /// </summary>
        void DatesToLocal();

        /// <summary>
        /// Преобразовать даты в объекте в формат UTC.
        /// </summary>
        void DatesToUtc();
    }
}
