﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Review.MesPackClient
{
    partial class BasicHttpClient
    {
        /// <summary>
        /// Выполнить запрос и вернуть результат в виде потока.
        /// Подходит для скачивания файлов.
        /// </summary>
        /// <param name="requestType">Тип запроса.</param>
        /// <param name="url">Адрес для запроса.</param>
        /// <param name="queryString">Query String.</param>
        /// <param name="cancellationToken">Токен для отмены запроса.</param>
        /// <returns>Поток в случае успеха, в противном случае null.</returns>
        public async Task<Stream> RequestStream(RequestTypes requestType,
            string url,
            string queryString,
            IProgress<int> progress,
            CancellationToken cancellationToken)
        {
            var requestUrl = ConstructUrl(url, queryString);

            _progressReportHelper.SetProgress(progress);

            var response = await ConstructResponseTask(requestType, requestUrl, null, cancellationToken);
            _progressReportHelper.ClearProgress();

            if (!response.IsSuccessStatusCode)
            {
                return null;
            }

            var s = await response.Content.ReadAsStreamAsync();
            return s;
        }

        /// <summary>
        /// Загрузить файл на сервер.
        /// </summary>
        /// <param name="url">Адрес для загрузки.</param>
        /// <param name="content">Тело запроса.</param>
        /// <param name="filePath">Путь в файлу.</param>
        /// <param name="fieldName">Название ключа для файла в теле.</param>
        /// <param name="queryString">Query String.</param>
        /// <param name="progress">Прогресс выполнения.</param>
        /// <param name="cancellationToken">Токен для отмены запроса.</param>
        /// <returns>True - если загрузка успешна, иначе false.</returns>
        public async Task<bool> UploadFile(
            string url,
            MultipartFormDataContent content,
            string filePath,
            string fieldName,
            string queryString = null,
            IProgress<int> progress = null,
            CancellationToken cancellationToken = default)
        {
            var response = await ProcessFileUpload(url, queryString, content, filePath, fieldName, progress, cancellationToken);
            var result = response.IsSuccessStatusCode;
            return result;
        }

        /// <summary>
        /// Загрузить файл на сервер.
        /// </summary>
        /// <typeparam name="TOutputDto">Тип ответной посылки.</typeparam>
        /// <param name="url">Адрес для загрузки.</param>
        /// <param name="content">Тело запроса.</param>
        /// <param name="filePath">Путь к файлу.</param>
        /// <param name="fieldName">Название ключа файла в теле.</param>
        /// <param name="defaultOutput">Результат при невыполнении запроса.</param>
        /// <param name="queryString">Query String.</param>
        /// <param name="progress">Прогресс выполнения задачи.</param>
        /// <param name="cancellationToken">Токен для отмены выполнения запроса.</param>
        /// <returns>При успешной загрузке возвращает DTO от сервера, при неудаче результат по умолчанию.</returns>
        public async Task<TOutputDto> UploadFile<TOutputDto>(
            string url,
            MultipartFormDataContent content,
            string filePath,
            string fieldName,
            TOutputDto defaultOutput = default,
            string queryString = null,
            IProgress<int> progress = null,
            CancellationToken cancellationToken = default)
        {
            var response = await ProcessFileUpload(url, queryString, content, filePath, fieldName, progress, cancellationToken);

            if (!response.IsSuccessStatusCode)
            {
                return defaultOutput;
            }

            var result = await DeserializeFromMessagePackAsync<TOutputDto>(response.Content);
            return result;
        }

        /// <summary>
        /// Отправить файл не сервер с помощью потока.
        /// </summary>
        /// <typeparam name="TOutputDto">Тип ответной посылки.</typeparam>
        /// <param name="url">Адрес для загрузки.</param>
        /// <param name="content">Составное тело запроса.</param>
        /// <param name="fileStream">Поток с файлом.</param>
        /// <param name="fieldName">Название поля ключа для файла в теле.</param>
        /// <param name="fileName">Имя файла.</param>
        /// <param name="defaultOutput">Результат, который вернётся при неудачном выполнении.</param>
        /// <param name="queryString">Query String.</param>
        /// <param name="progress">Прогресс выполнения задачи.</param>
        /// <param name="cancellationToken">Токен для отмены запроса.</param>
        /// <returns>При успешной загрузке возвращает DTO от сервера, при неудаче - результат по умолчанию.</returns>
        public async Task<TOutputDto> UploadStream<TOutputDto>(
            string url,
            MultipartFormDataContent content,
            Stream fileStream,
            string fieldName,
            string fileName,
            TOutputDto defaultOutput = default,
            string queryString = null,
            IProgress<int> progress = null,
            CancellationToken cancellationToken = default)
        {
            var response = await ProcessStreamUpload(url, queryString, content, fileStream, fieldName, fileName, progress, cancellationToken);

            if (!response.IsSuccessStatusCode)
            {
                return defaultOutput;
            }

            var result = await DeserializeFromMessagePackAsync<TOutputDto>(response.Content);
            return result;
        }

        /// <summary>
        /// Отправить файл не сервер с помощью потока.
        /// </summary>
        /// <param name="url">Адрес для загрузки.</param>
        /// <param name="content">Составное тело запроса.</param>
        /// <param name="fileStream">Поток с файлом.</param>
        /// <param name="fieldName">Название поля ключа для файла в теле.</param>
        /// <param name="fileName">Имя файла.</param>
        /// <param name="queryString">Query String.</param>
        /// <param name="progress">Прогресс выполнения задачи.</param>
        /// <param name="cancellationToken">Токен для отмены запроса.</param>
        /// <returns>True - если загрузка успешна, иначе false.</returns>
        public async Task<bool> UploadStream(
            string url,
            MultipartFormDataContent content,
            Stream fileStream,
            string fieldName,
            string fileName,
            string queryString = null,
            IProgress<int> progress = null,
            CancellationToken cancellationToken = default)
        {
            var response = await ProcessStreamUpload(url, queryString, content, fileStream, fieldName, fileName, progress, cancellationToken);
            var result = response.IsSuccessStatusCode;
            return result;
        }

        /// <summary>
        /// Выполнить загрузку файла на сервер.
        /// </summary>
        /// <param name="url">Адрес для загрузки.</param>
        /// <param name="queryString">Query String.</param>
        /// <param name="content">Составной запрос</param>
        /// <param name="filePath">Имя файла</param>
        /// <param name="fieldName">Название поля ключа для файла в составном теле.</param>
        /// <param name="progress">Прогресс выполнения задачи.</param>
        /// <param name="cancellationToken">Токен для отмены запроса.</param>
        /// <returns>Ответ сервера.</returns>
        private async Task<HttpResponseMessage> ProcessFileUpload(
            string url,
            string queryString,
            MultipartFormDataContent content,
            string filePath,
            string fieldName,
            IProgress<int> progress,
            CancellationToken cancellationToken)
        {
            var fileName = Path.GetFileName(filePath);
            return await ProcessStreamUpload(url, queryString, content, File.Open(filePath, FileMode.Open), fieldName, fileName, progress, cancellationToken);
        }

        /// <summary>
        /// Выполнить загрузку файла на сервер с помощью потока.
        /// </summary>
        /// <param name="url">Адрес для загрузки</param>
        /// <param name="queryString">Query String.</param>
        /// <param name="content">Тело составной запроса.</param>
        /// <param name="fileStream">Поток с файлом.</param>
        /// <param name="fieldName">Название поля содержащее файл в теле запроса.</param>
        /// <param name="fileName">Имя файла.</param>
        /// <param name="progress">Прогресс выполнения задачи.</param>
        /// <param name="cancellationToken">Токен для отмены запроса.</param>
        /// <returns>Ответ сервера.</returns>
        private async Task<HttpResponseMessage> ProcessStreamUpload(
            string url,
            string queryString,
            MultipartFormDataContent content,
            Stream fileStream,
            string fieldName,
            string fileName,
            IProgress<int> progress,
            CancellationToken cancellationToken)
        {
            var requestUrl = ConstructUrl(url, queryString);

            _progressReportHelper.SetProgress(progress);

            var stream = new StreamContent(fileStream);
            content.Add(stream, fieldName, fileName);

            var response = await ConstructResponseTask(RequestTypes.Post, requestUrl, content, cancellationToken);
            stream.Dispose();
            _progressReportHelper.ClearProgress();

            return response;
        }
    }
}
