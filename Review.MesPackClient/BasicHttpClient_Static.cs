﻿using MessagePack;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace Review.MesPackClient
{
    partial class BasicHttpClient
    {
        /// <summary>
        /// Сформировать тело запроса, сериализовав его в MessagePack.
        /// </summary>
        /// <param name="value">Объект, который будет телом запроса.</param>
        /// <returns>Сформированное тело запроса.</returns>
        public static async Task<HttpContent> SerializeToMessagePackAsync(object value)
        {
            var ms = new MemoryStream();
            await MessagePackSerializer.SerializeAsync(ms, value);

            var result = new ByteArrayContent(ms.ToArray());
            result.Headers.ContentType = new MediaTypeWithQualityHeaderValue("application/x-msgpack");
            return result;
        }

        /// <summary>
        /// Сформировать тело запроса, сериализовав его в MessagePack.
        /// </summary>
        /// <typeparam name="T">Тип объекта для тела.</typeparam>
        /// <param name="value">Объект, который будет телом запроса.</param>
        /// <returns>Сформированное тело запроса.</returns>
        public static HttpContent SerializeToMessagePack<T>(T value)
        {
            var array = MessagePackSerializer.Serialize(value);

            var result = new ByteArrayContent(array);
            result.Headers.ContentType = new MediaTypeWithQualityHeaderValue("application/x-msgpack");
            return result;
        }

        /// <summary>
        /// Десериализовать тело запроса из MessagePack формата.
        /// </summary>
        /// <typeparam name="T">Тип объекта на выходе.</typeparam>
        /// <param name="content">Тело Http сообщения.</param>
        /// <returns>Объект, представляющий ответ.</returns>
        public static async Task<T> DeserializeFromMessagePackAsync<T>(HttpContent content)
        {
            var s = await content.ReadAsStreamAsync();
            var result = await MessagePackSerializer.DeserializeAsync<T>(s);
            return result;
        }

        /// <summary>
        /// Создает Query String.
        /// </summary>
        /// <param name="collection">Список ключей и их значений.</param>
        /// <returns>Query String.</returns>
        public static string GetQueryString(ICollection<KeyValuePair<string, object>> collection)
        {
            string result = string.Join("&",
                         collection.Select(a => a.Key+ "=" + HttpUtility.UrlEncode(a.Value.ToString())));

            return result;
        }
        /// <summary>
        /// Создает Query String.
        /// </summary>
        /// <param name="key">Название ключа. </param>
        /// <param name="value">Значение ключа.</param>
        /// <returns>Query String.</returns>
        public static string GetQueryString(object key, object value)
        {
            string result = $"{key}={HttpUtility.UrlEncode(value.ToString())}";
            return result;
        }
    }
}
