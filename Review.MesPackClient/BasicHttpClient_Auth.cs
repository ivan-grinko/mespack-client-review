﻿using IdentityModel.Client;
using IdentityModel.OidcClient;
using IdentityModel.OidcClient.Browser;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Security.Principal;
using System.Text;
using System.Threading.Tasks;

namespace Review.MesPackClient
{
    partial class BasicHttpClient
    {
        /// <summary>
        /// Осуществляет авторизацию пользователя.
        /// </summary>
        /// <param name="authority">URL сервера авторизации.</param>
        /// <param name="clientId">ID клиента.</param>
        /// <param name="scope">Scope API.</param>
        /// <param name="redirectUri">URL для перенаправления после завершения входа.</param>
        /// <param name="postLogoutRedirectUri">URL для перенаправления после выхода.</param>
        /// <param name="browser">Интерфейс браузера.</param>
        /// <returns>Данные пользователя или null, если вход не выполнен.</returns>
        public async Task<IIdentity> Login(string authority, string clientId, string scope, string redirectUri, string postLogoutRedirectUri, IBrowser browser)
        {
            // Параметры OpenId Connect.
            var options = new OidcClientOptions
            {
                Authority = authority,
                ClientId = clientId,
                //ClientSecret = clientSecret,
                Scope = "openid profile offline_access " + scope,
                FilterClaims = false,
                Browser = browser,

                Flow = OidcClientOptions.AuthenticationFlow.AuthorizationCode,

                //RedirectUri = "http://127.0.0.1:45656",
                RedirectUri = redirectUri,
                PostLogoutRedirectUri = postLogoutRedirectUri,
                //ResponseMode = OidcClientOptions.AuthorizeResponseMode.Redirect,
                ResponseMode = OidcClientOptions.AuthorizeResponseMode.FormPost,
                LoadProfile = true, 
            };

            _oidcClient = new OidcClient(options);

            // Вход.
            _login = await _oidcClient.LoginAsync(new LoginRequest());

            if (_login.IsError)
            {
                return null;
            }

            // Получение токенов доступа.
            _currentAccessToken = _login.AccessToken;
            _currentRefreshToken = _login.RefreshToken;

            // Установка токена доступа HTTP клиенту.
            _client.SetBearerToken(_currentAccessToken);

            // Запуск процедуры обновления токенов.
            var refreshTokenInterval = (_login.AccessTokenExpiration - _login.AuthenticationTime).TotalMilliseconds;
            refreshTokenInterval *= 0.9;
            _tokenRefreshTimer.Interval = refreshTokenInterval;
            _tokenRefreshTimer.Start();

            return _login.User.Identity;
        }

        /// <summary>
        /// Производит выход пользователя.
        /// </summary>
        /// <returns></returns>
        public async Task Logout()
        {
            _tokenRefreshTimer.Stop();
            await _oidcClient?.LogoutAsync();
        }

        /// <summary>
        /// Обновление токена доступа.
        /// </summary>
        /// <returns></returns>
        private async Task RefreshToken()
        {
            _tokenRefreshTimer.Stop();
            var result = await _oidcClient.RefreshTokenAsync(_currentRefreshToken);

            if (result.IsError)
            {
                System.Diagnostics.Debug.WriteLine($"Error: {result.Error}");
            }
            else
            {
                _currentRefreshToken = result.RefreshToken;
                _currentAccessToken = result.AccessToken;

                _client.SetBearerToken(_currentAccessToken);

                _tokenRefreshTimer.Start();

                //System.Diagnostics.Debug.WriteLine($"access token:   {result.AccessToken}");
                //System.Diagnostics.Debug.WriteLine($"refresh token:  {result?.RefreshToken ?? "none"}");
            }
        }
    }
}
